/**
 * Die Klasse Drucker gibt die Zahl, die aktuell im Speicher ist, zurueck.
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */

public class Drucker extends Thread {
	private Speicher speicher;

	Drucker(Speicher s) {
		this.speicher = s;
	}

	
	@Override
	public void run() {
		
		synchronized (speicher){
			if(!speicher.isHatWert()){
				try {
					speicher.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		/*
		 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen
		 * Leerzeichen.
		 */
		while (true) {
			try {
				synchronized (speicher){
					System.out.print(speicher.getWert() + " ");
					speicher.wait();
				}

	
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
